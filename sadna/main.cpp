#include <iostream>
#include "windows_shell.h"
#include <string>

using std::cout;
using std::cin;

int main()
{
	string currTask;
	windows_shell wind;
	while (true)
	{
		//getting input
		cout << ">> ";
		std::getline(cin, currTask);
		
		//checking input
		Helper::trim(currTask);
		if (currTask.compare("quit()") == 0)
		{
			break;
		}
		else if (currTask.empty())
		{
			continue;
		}
		//operating on the input
		wind.getInput(currTask);
	}

}