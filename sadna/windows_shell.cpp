#include "windows_shell.h"


windows_shell::windows_shell()
{
}
windows_shell::~windows_shell()
{
}

//gets an linput line and chooses the right fanction to call
void windows_shell::getInput(std::string task)
{
	vector<string> argList = Helper::get_words(task);

	if (argList[0].compare("pwd") == 0)
	{
		showPath();
	}
	else if (argList[0].compare("ls") == 0)
	{
		listFiles();
	}
	else if (argList[0].compare("cd") == 0)
	{
		if (argList.size() > 1)
		{
			string path = argList[1];
			//concatanit the arguments to one path
			for (int i = 2; i < argList.size(); i++)
			{
				path += " ";
				path += argList[i];
			}
			changeDir(path);
		}
		else
		{
			cout << "not enough arguments\n";
		}
	}
	else if (argList[0].compare("create") == 0)
	{
		if (argList.size() > 1)
		{
			createFolder(argList[1]);
		}
		else
		{
			cout << "not enough arguments\n";
		}

	}
	else if (argList[0].compare("secret") == 0) 
	{
		secret();
	}
	else
	{
		runExe(task);
	}
}

//gets the current directory and prints it
void windows_shell::showPath()
{
	char pathName [MAX_PATH]= { 0 };
	
	//gets the current directory
	int status = GetCurrentDirectoryA(MAX_PATH, pathName);
	if (status == 0)
	{
		int errorCode = GetLastError();
		cout << "ERROR: error code " << errorCode << "\n";
	}
	else
	{
		cout << "Path: " << pathName << '\n';
	}
}

//changes the directory of the program
void windows_shell::changeDir(string path)
{
	//set the path
	if (!SetCurrentDirectoryA(path.c_str()))
	{
		int errorCode = GetLastError();
		cout << "ERROR: error code " << errorCode << "\n";
	}
}

//creates a file in the current directory
void windows_shell::createFolder(string fileName)
{
	//creating the file
	HANDLE hand = CreateFileA(fileName.c_str(),
					GENERIC_READ | GENERIC_WRITE,
					FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL,
					CREATE_ALWAYS,
					FILE_ATTRIBUTE_NORMAL,
					NULL);

	if (hand == INVALID_HANDLE_VALUE)
	{
		int errorCode = GetLastError();
		cout << "ERROR: error code " << errorCode << '\n';
	}
	else
	{
		CloseHandle(hand);
	}
}

//prints all the files in the current folder
bool windows_shell::listFiles()
{
	TCHAR path[MAX_PATH];
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	HANDLE hfind;
	DWORD dwError = 0;

	//getting the current directory
	int status = GetCurrentDirectoryA(MAX_PATH, path);
	if (status == 0)
	{
		dwError = GetLastError();
		cout << "ERROR: error code " << dwError << "\n";
		return 0;
	}
	else
	{
		//adding \\* to the string 
		string temp;
		temp = path;
		temp += "\\*";
		strncpy_s(path,temp.c_str(),temp.size());

		//finding the first filr in the folder
		hfind = FindFirstFile(path, &ffd);
		if (INVALID_HANDLE_VALUE == hfind)
		{
			dwError = GetLastError();
			cout << "ERROR: error code " << dwError << "\n";
			return 0;
		}

		//printing and finding the next file until there are no more files in the folder.
		do
		{
			cout << ffd.cFileName << '\n';
		} while (FindNextFile(hfind, &ffd) != 0);

		dwError = GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			//if the problem was different then no more files
			cout << "ERROR: error code " << dwError << "\n";
		}
		
		FindClose(hfind);
		return dwError;
	}
}

//loads a library, call the function TheAnswerToLifeTheUniverseAndEverything() and prints the answer
int windows_shell::secret()
{
	MYPROC func = NULL;
	//loading the library
	HMODULE hLib = LoadLibraryA(TEXT(SECRET_PATH));

	if (hLib != NULL)
	{
		//getting the address of the function
		func = (MYPROC)GetProcAddress(hLib, TEXT(SECRET_FUNC));
		if (func == NULL)
		{
			int errorCode = GetLastError();
			cout << "ERROR: error code " << errorCode << "\n";
			return errorCode;
		}
		//calling and printing the function
		int ans = func();
		cout << ans << "\n";
	}
	FreeLibrary(hLib);
	return 0;
}

/*
tries to run the string as a exe file

WaitForSingleObject - stops the thread until the object spesified is closed.
GetExitCodeProcess - gets the exit code of a terminated process to see if it ended with an error.
*/
void windows_shell::runExe(string exeName)
{
	bool res = false;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD exitCode;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	//creating the process
	res = CreateProcessA(exeName.c_str(),
		NULL,
		NULL,
		NULL,
		false,
		0,
		NULL,
		NULL,
		&si,
		&pi);

	if (!res)
	{
		int temp = GetLastError();
		if (temp == ERROR_FILE_NOT_FOUND)
		{
			cout << exeName << " is not an executable file in the current directory\n";
		}
		else
		{
			cout << "ERROR: CreateProcess failed.	CodeError " << temp << "\n";
		}
		
	}
	else
	{
		//waiting until the process ends or is put into sleep
		WaitForSingleObject(pi.hProcess, INFINITE);

		//getting its exit code
		if (GetExitCodeProcess(pi.hProcess, &exitCode))
		{
			cout << exeName << " exited with exitCode: " << exitCode << "\n";
		}

		//Close process and thread handles. 
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
		
	}
}
