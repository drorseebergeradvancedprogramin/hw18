#pragma once
#include <Windows.h>
#include <iostream>
#include "Helper.h"
#include <string>
#include <exception>

#define SECRET_PATH "Secret.dll"
#define SECRET_FUNC "TheAnswerToLifeTheUniverseAndEverything"
typedef  int (WINAPI *MYPROC)(void);
using std::cout;
using std::string;


class windows_shell
{
public:
	void getInput(string task);
	windows_shell();
	~windows_shell();

private:
	void showPath();
	void changeDir(string path);
	void createFolder(string fileName);
	bool listFiles();
	int secret();
	void runExe(string exeName);
};

